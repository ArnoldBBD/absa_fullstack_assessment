using ABSA_Phonebook_Backend.Controllers;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Xunit;

namespace ABSA_PhonebookAPIUnitTests
{
    public class PhonebookControllerTest
    {
        private CPhonebook _phonebook;
        private readonly string _phonebookId;
        private readonly string _searchString;
        private readonly PhonebookController _phonebookController;
        private readonly Mock<IPhonebookRepository> _mockPhonebookRepository;


        public PhonebookControllerTest()
        {
            _phonebook = null;
            _phonebookId = Guid.NewGuid().ToString();
            _searchString = "JustACustomSearchString";
            _mockPhonebookRepository = new Mock<IPhonebookRepository>();
            _mockPhonebookRepository.Setup(p => p.Update(It.IsAny<CPhonebook>())).Returns(Task.CompletedTask).Callback<CPhonebook>(x => _phonebook = x);

            _phonebookController = new PhonebookController(_mockPhonebookRepository.Object);
        }

        /// <summary>
        /// Test to check that an invalid id will not be stored and that the api will return a bad request.
        /// /api/Phonebook
        /// </summary>
        /// <returns>BadRequest</returns>
        [Fact]
        public async Task CreatePhonebook_ReturnsBadRequest_WhenIdIsInvalid()
        {

            //Arrange
            _phonebookController.ModelState.AddModelError("error", "Invalid Id");
            var newPhonebook = new CPhonebook { Id = "", name = "Test Phonebook Name", description = "Test Phonebook Description" };

            //Act
            var result = await _phonebookController.CreatePhonebook(newPhonebook);


            //Assert
            var requestResult = Assert.IsType<BadRequestResult>(result);

        }

        /// <summary>
        /// Test to store a valid phonebook entry and return the instance created.
        /// /api/Phonebook
        /// </summary>
        /// <returns>CPhonebook</returns>
        [Fact]
        public async Task CreatePhonebook_ReturnsPhonebook_WhenValid()
        {
            //Arrange
            var newPhonebook = new CPhonebook { Id = _phonebookId, name = "Test Phonebook Name", description = "Test Phonebook Description" };
            _mockPhonebookRepository.Setup(p => p.Create(It.IsAny<CPhonebook>())).ReturnsAsync(newPhonebook);

            //Act
            var result = await _phonebookController.CreatePhonebook(newPhonebook);

            //Assert
            var requestResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<CPhonebook>(requestResult.Value);
        }

        /// <summary>
        /// API should return not found results when the entered phonebook name was not found
        /// /api/Phonebook/{searchString}
        /// </summary>
        /// <returns>NotFound</returns>
        [Fact]
        public async Task GetPhonebook_ShouldReturnNotFound_StringNotFound()
        {

            //Arrange
            _mockPhonebookRepository.Setup(p => p.GetAll());

            //Act
            var results = await _phonebookController.GetPhonebookBySearch(_searchString);

            //Assert
            var requestResult = Assert.IsType<NotFoundObjectResult>(results);
            Assert.Equal(_searchString, requestResult.Value);
        }

        /// <summary>
        /// API should return a valid list of phonebooks when the entered phonebook name was found
        /// /api/Phonebook/{searchString}
        /// </summary>
        /// <returns>List-CPhonebook</returns>
        [Fact]
        public async Task GetPhonebook_ShouldReturnPhonebooks_StringWasFound()
        {

            //Arrange
            string searchString = "BBD Contacts";
            var list = new List<CPhonebook>();
            _mockPhonebookRepository.Setup(p => p.GetAll()).Returns(Task.FromResult(list));
            var expected = list.Where(p => p.name.ToLower().Contains(_searchString));

            //Act
            var results = await _phonebookController.GetPhonebookBySearch(searchString);

            //Assert
            var requestResult = Assert.IsType<OkObjectResult>(results);
            Assert.Equal(expected, requestResult.Value);
        }

        [Fact]
        public async Task UpdatePhonebook_ReturnsBadRequest_IdDoesNotMatch()
        {
            //Arrange
            _mockPhonebookRepository.Setup(p => p.Update(It.IsAny<CPhonebook>())).ThrowsAsync(new Exception());

            //Act
            var result = await _phonebookController.UpdatePhonebook(new CPhonebook());

            //Assert
            var requestResult = Assert.IsType<BadRequestResult>(result);
            Assert.NotNull(requestResult);
        }

    }
}
