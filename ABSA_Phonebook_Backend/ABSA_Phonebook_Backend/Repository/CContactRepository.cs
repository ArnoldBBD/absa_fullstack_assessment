﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Models;
using ABSA_Phonebook_Backend.Services;
using Microsoft.Azure.Documents;

namespace ABSA_Phonebook_Backend.Repository
{

    public class CContactRepository : CosmosDbRepository<CContact>, IContactRepository
    {
        public CContactRepository(ICosmosDbFactory dbFactory) : base(dbFactory)
        { }

        public override string CollectionName { get; } = "entry";
        public override string Id(CContact entity)
        {
            return Guid.NewGuid().ToString();
        }

        public override PartitionKey EntryPartitionKey(string entryId)
        {
            return new PartitionKey(entryId.Split(':')[0]);
        }
    }
}
