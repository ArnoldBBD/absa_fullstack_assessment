﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Models;
using ABSA_Phonebook_Backend.Services;
using Microsoft.Azure.Documents;

namespace ABSA_Phonebook_Backend.Repository
{

    public class CPhonebookRepository : CosmosDbRepository<CPhonebook>, IPhonebookRepository
    {
        public CPhonebookRepository(ICosmosDbFactory dbFactory) : base(dbFactory)
        {  }

        public override string CollectionName { get; } = "phonebook";
        public override string Id(CPhonebook entity)
        {
            return Guid.NewGuid().ToString();
        }

        public override PartitionKey EntryPartitionKey(string entryId)
        {
            return new PartitionKey(entryId.Split(':')[0]);
        }
    }
}
