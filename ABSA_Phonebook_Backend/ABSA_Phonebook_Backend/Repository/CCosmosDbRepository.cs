﻿using ABSA_Phonebook_Backend.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Models;
using ABSA_Phonebook_Backend.Services;
using Microsoft.AspNetCore.Rewrite.Internal.IISUrlRewrite;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace ABSA_Phonebook_Backend.Repository
{

    /// <summary>
    /// CosmosDb Repository handle all the logical crud operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CosmosDbRepository<T> : IRepository<T>, ICollectionInfo<T> where T : CEntry
    {
        private readonly ICosmosDbFactory cosmosDbFactoryInstance;

        protected CosmosDbRepository(ICosmosDbFactory _cosmosDbFactoryInstance)
        {
            cosmosDbFactoryInstance = _cosmosDbFactoryInstance;
        }

        public async Task<List<T>> GetAll()
        {
            try
            {
                var cosmosDbClient = cosmosDbFactoryInstance.GetService(CollectionName);
                var docs = await cosmosDbClient.ReadDocuments();

                List<T> genericList = new List<T>();

                if (docs != null)
                {
                    foreach (var doc in docs)
                    {
                        genericList.Add(JsonConvert.DeserializeObject<T>(doc.ToString()));
                    }
                }
                return genericList;

            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new Exception(e.Message);
                }

                throw;
            }
        }

      
       

        public async Task<T> Create(T entity)
        {
            try
            {
                entity.Id = Id(entity);
                var cosmosDbClient = cosmosDbFactoryInstance.GetService(CollectionName);
                var document = await cosmosDbClient.CreateDocument(entity);
                return JsonConvert.DeserializeObject<T>(document.ToString());
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.Conflict)
                {
                    throw new Exception("Entry not found.");
                }

                throw;
            }
        }

        public async Task Update(T entity)
        {
            try
            {
                var cosmosDbClient = cosmosDbFactoryInstance.GetService(CollectionName);
                await cosmosDbClient.UpdateDocument(entity, entity.Id);
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new Exception("Entry not found.");
                }

                throw;
            }
        }

        public async Task Delete(T entity)
        {
            try
            {
                var cosmosDbClient = cosmosDbFactoryInstance.GetService(CollectionName);
                await cosmosDbClient.DeleteDocument(entity.Id, new RequestOptions()
                    { PartitionKey = new PartitionKey(Undefined.Value) });
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new Exception("Entry not found.");
                }

                throw new Exception(e.ToString());
            }
        }

        public abstract string CollectionName { get; }
        public virtual string Id(T entry) => Guid.NewGuid().ToString();

        public virtual PartitionKey EntryPartitionKey(string entryId) => null;
    }
}
