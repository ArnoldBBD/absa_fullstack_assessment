﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Models;

namespace ABSA_Phonebook_Backend.Interfaces
{
    /// <summary>
    /// IPhonebookRepository will be used when phonebook APIs implement functions that contacts doesn't.
    /// </summary>
    public interface IPhonebookRepository : IRepository<CPhonebook>
    {
        
    }
}
