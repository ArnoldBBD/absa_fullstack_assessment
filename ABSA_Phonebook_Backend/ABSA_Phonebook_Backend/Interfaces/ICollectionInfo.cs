﻿using ABSA_Phonebook_Backend.Models;
using Microsoft.Azure.Documents;
using Newtonsoft.Json.Converters;

namespace ABSA_Phonebook_Backend.Interfaces
{

    /// <summary>
    /// Cosmos Db collection info
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICollectionInfo<in T> where T : CEntry
    {
        string CollectionName { get; }
        string Id(T entry);

        PartitionKey EntryPartitionKey(string entryId);

    }
}
