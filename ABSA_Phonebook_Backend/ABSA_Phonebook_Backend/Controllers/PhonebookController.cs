﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Interfaces;
using ABSA_Phonebook_Backend.Models;
using ABSA_Phonebook_Backend.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.SystemFunctions;

namespace ABSA_Phonebook_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhonebookController : ControllerBase
    {

        private readonly IPhonebookRepository _phonebookRepository;

        /// <summary>
        /// Contructor to assign an IPhonebookRepository instance
        /// </summary>
        /// <param name="phonebookRepository">New/Existing IPhonebookRepository instance</param>
        public PhonebookController(IPhonebookRepository phonebookRepository)
        {
            this._phonebookRepository = phonebookRepository;
        }


        /// <summary>
        /// Creating a new Phonebook entry
        /// </summary>
        /// <param name="newPhonebook">JSON New Phonebook entry document</param>
        [HttpPost]
        public async Task<ActionResult> CreatePhonebook([FromBody] CPhonebook newPhonebook)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

           var phonebook = await _phonebookRepository.Create(newPhonebook);

            return Ok(phonebook);
        }

        /// <summary>
        /// Fetch all Phonebook documents 
        /// </summary>
        /// <returns>List-CPhonebook</returns>
        [HttpGet]
        public async Task<ActionResult> GetPhonebooks()
        {
            try
            {
                return Ok(await _phonebookRepository.GetAll());
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

            
        }

        /// <summary>
        /// Fetch all phonebook documents and filter by search string
        /// </summary>
        /// <param name="searchString">STRING search phrase</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{searchString}")]
        public async Task<ActionResult> GetPhonebookBySearch(string searchString)
        {
            try
            {
                var phonebooks = await _phonebookRepository.GetAll();
                var resultList = phonebooks.Where(p =>
                    p.name.ToLower().Contains(searchString.ToLower()));

                return Ok(resultList);

            }
            catch (Exception)
            {
                return NotFound(searchString);
            }


        }

        /// <summary>
        /// Updating an existing Phonebook entry
        /// </summary>
        /// <param name="phonebookEntry">JSON Phonebook entry document</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdatePhonebook([FromBody] CPhonebook phonebookEntry)
        {
         

            try
            {
                if (phonebookEntry.Id == null)
                {
                    return BadRequest();
                }
                else
                {
                    await _phonebookRepository.Update(phonebookEntry);
                    return Ok();
                }
                
            }
            catch { return NotFound(); }
             
        }

        /// <summary>
        /// Deleting a Phonebook entry
        /// </summary>
        /// <param name="id">Phonebook it to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> DeletePhonebook(string id)
        {
            try
            {
                var phonebooks = await _phonebookRepository.GetAll();
                var phonebook = phonebooks.Where(p => p.Id.Equals(id)).AsEnumerable().Single();

                var phonebookToDelete = new CPhonebook() { Id = phonebook.Id, name = phonebook.name, description = phonebook.description };

                await _phonebookRepository.Delete(phonebookToDelete);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }

          
        }
    }

    
}