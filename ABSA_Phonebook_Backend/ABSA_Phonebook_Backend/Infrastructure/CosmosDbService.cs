﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ABSA_Phonebook_Backend.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace ABSA_Phonebook_Backend.Services
{
    public class CosmosDbService : ICosmosDbService
    {
        private readonly string databaseName;
        private readonly string collectionName;
        private readonly IDocumentClient documentClient;

        public CosmosDbService(string _databaseName, string _collectionName, IDocumentClient _documentClient)
        {
            databaseName = _databaseName ?? throw new ArgumentNullException(nameof(_databaseName));
            collectionName = _collectionName ?? throw new ArgumentNullException(nameof(_collectionName));
            documentClient = _documentClient ?? throw new ArgumentNullException(nameof(_documentClient));
        }

        /// <summary>
        /// Create a new document in cosmos db
        /// </summary>
        /// <param name="document">Model-document</param>
        /// <param name="options">Specified options for the action</param>
        /// <param name="disableGUID">Disable automatic id generation</param>
        /// <param name="cancellationToken">Propagates notification that operations should be canceled.</param>
        /// <returns></returns>
        public async Task<Document> CreateDocument(object document, RequestOptions options = null, bool disableGUID = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await documentClient.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), document, options,
                disableGUID, cancellationToken);
        }

        /// <summary>
        /// Delete a specified document 
        /// </summary>
        /// <param name="docId">Model id</param>
        /// <param name="options">Specified options for the action</param>
        /// <param name="cancellationToken">Propagates notification that operations should be canceled.</param>
        /// <returns></returns>
        public async Task<Document> DeleteDocument(string docId, RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await documentClient.DeleteDocumentAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, docId), options, cancellationToken);
        }

        /// <summary>
        /// Read a document from CosmosDb. 
        /// Bug- Currently a bug with the UriFactory.ReadDocument on .net core so I used document Query. 
        /// </summary>
        /// <param name="options">Specified options for the action</param>
        /// <param name="cancellationToken">Propagates notification that operations should be canceled.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Document>> ReadDocuments(RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var option = new FeedOptions { EnableCrossPartitionQuery = true };

            return documentClient.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), option).AsEnumerable();
        }


        /// <summary>
        /// Update a specified document in CosmosDb
        /// </summary>
        /// <param name="document">Model document</param>
        /// <param name="docId">Model Id</param>
        /// <param name="options">Model Document</param>
        /// <param name="cancellationToken">Propagates notification that operations should be canceled.</param>
        /// <returns></returns>
        public async Task<Document> UpdateDocument(object document, string docId, RequestOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
          
           return await documentClient.ReplaceDocumentAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, docId), document, options,
                cancellationToken);
        }


    }
}
