﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Services
{
    public class CCosmosDbOptions
    {
        public string DatabaseName { get; set; }
        public List<CCollectionInfo> CollectionNames { get; set; }

        /// <summary>
        /// Returns the database name and the related documents
        /// </summary>
        /// <param name="databaseName">Refers to the ABSA PhonebookDB</param>
        /// <param name="collectionNames">Refer to the contact and phonebook documents</param>
        public void Deconstruct(out string databaseName, out List<CCollectionInfo> collectionNames)
        {

            databaseName = DatabaseName;
            collectionNames = CollectionNames;
        }
    }

    public class CCollectionInfo
    {
        /// <summary>
        /// Document collection name
        /// </summary>
        /// <example>contact</example>
        public string Name { get; set; }

        /// <summary>
        /// Document collection partition key value
        /// </summary>
        /// <example>/contactId</example>
        public string PartitionKey { get; set; }
    }
}
