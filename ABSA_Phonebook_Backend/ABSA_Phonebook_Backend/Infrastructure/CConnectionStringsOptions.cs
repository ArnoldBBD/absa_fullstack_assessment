﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Services
{

    public enum ConnectionMode
    {
        Azure,
        Emulator
    }
    public class CConnectionStringsOptions
    {
        public ConnectionMode Mode { get; set; }
        public CConnectionStringOptions Azure { get; set; }
        public CConnectionStringOptions Emulator { get; set; }

        /// <summary>
        /// Uses azure or else we will use the CosmosDB Emulator (Typically for local development purposes and cost saving.)
        /// </summary>
        public CConnectionStringOptions ActiveConnectionString => Mode == ConnectionMode.Azure ? Azure : Emulator;
    }
}
