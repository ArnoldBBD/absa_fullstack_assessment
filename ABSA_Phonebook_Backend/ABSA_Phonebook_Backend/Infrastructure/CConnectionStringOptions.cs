﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Services
{
    public class CConnectionStringOptions
    {

        public Uri ServiceEndpoint { get; set; }
        public string AuthKey { get; set; }

        /// <summary>
        /// Contructor to assign ServiceEndpoint and AuthKey
        /// </summary>
        /// <param name="serviceEndpoint">Azure service endpoint key- On azure known as the URI</param>
        /// <param name="authKey">Azure Authentication key- On azure known as the Primary Key</param>
        public void Deconstruct(out Uri serviceEndpoint, out string authKey)
        {
            serviceEndpoint = ServiceEndpoint;
            authKey = AuthKey;
        }
    }
}
