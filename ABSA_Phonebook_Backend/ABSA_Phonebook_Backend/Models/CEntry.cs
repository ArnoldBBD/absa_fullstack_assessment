﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABSA_Phonebook_Backend.Models
{
    /// <summary>
    /// Base model used for all entries
    /// </summary>
    public abstract class CEntry
    {
        public string Id { get; set; }
    }
}
