import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettingsProvider } from '../app-settings';
import { Observable } from 'rxjs/Observable';
import { Phonebook } from '../../models/phonebook';

/*
  Generated class for the DonationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PhonebookProvider {

public _phonebookItems: Observable<Phonebook[]>;

  constructor(private _http: HttpClient, private _appsettings: AppSettingsProvider) {

  }

getPhonebooks(): Observable<Phonebook[]> { 
        this._phonebookItems = this._http.get<Phonebook[]>(`${this._appsettings.APIEndPoint}/phonebook`);
        return this._phonebookItems;
   }

getPhonebookBySearch(search: String): Observable<Phonebook[]> { 
    return this._http.get<Phonebook[]>(`${this._appsettings.APIEndPoint}/phonebook/` + search);
}

getPhonebookByID(Id: String): Observable<Phonebook> { 
  return this._http.get<Phonebook>(`${this._appsettings.APIEndPoint}/phonebook/` + Id);
}

createPhonebook(p: Phonebook){
  return this._http.post(`${this._appsettings.APIEndPoint}/phonebook`,p);
}

deletePhonebook(id: string) { 
  return this._http.delete(`${this._appsettings.APIEndPoint}/phonebook/` + id);
}

updatePhonebook(p: Phonebook) { 
  return this._http.put(`${this._appsettings.APIEndPoint}/phonebook/`, p);
}

}
