import { Injectable, ChangeDetectionStrategy } from '@angular/core';

@Injectable()
export class Util {

  constructor() {
  
    console.log('Hello UtilProvider Provider');
  }

  static pathAvatar(): string {
    var AvatarArray = [
      "Bentley.png",
      "Chloe.png",
      "Dylan.png",
      "Elliot.png",
      "Gabriel.png",
      "Grace.png",
      "Raouf.png",
      "Savannah.png"
    ];

    var randomAvatar = AvatarArray[Math.floor(Math.random()*AvatarArray.length)];

    return './assets/avatar/' + randomAvatar;
  }

}
