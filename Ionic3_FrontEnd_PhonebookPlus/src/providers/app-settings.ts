import { Injectable } from '@angular/core';

@Injectable()
export class AppSettingsProvider {
  
  get APIEndPoint () : string
  {
    return "https://absaphonebookbackend.azurewebsites.net/api/";
  }
  constructor() {}

}