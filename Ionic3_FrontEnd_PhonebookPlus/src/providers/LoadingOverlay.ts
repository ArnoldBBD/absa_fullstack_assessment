import { Injectable } from '@angular/core';
import {LoadingController} from 'ionic-angular';

@Injectable()
export class LoadingOverlay {
 
    constructor(private _loadingCtrl: LoadingController) { }

    public ShowMessage(msg: string)  : any {
   
        let loading = this._loadingCtrl.create({
            content: `${msg}...`
        });

        loading.present();  
 
        return loading;
    }
 

}