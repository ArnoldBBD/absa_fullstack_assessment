export class Contact {
    id?: string;
    contactName?: string;
    phoneNumber?: string;
    phonebookId?: string;
    avatar?: string;
  }