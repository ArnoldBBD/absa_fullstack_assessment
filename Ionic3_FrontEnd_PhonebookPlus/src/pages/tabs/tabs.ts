import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Tabs, Events } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild(Tabs) tabs: Tabs;

  tab1Root = HomePage;
  tab2Root = ContactPage;
  tab3Root = AboutPage;
  tab2Params = {page: this, type: "", id: "5"};

  constructor(events: Events) {
      events.subscribe('change-tab', (tabs, _type, _id) => {
      this.tab2Params.type = _type;
      this.tab2Params.id = _id;
      this.tabs.select(tabs);
    
    });
  }

  public setType(){
     this.tab2Params.type = "";
     this.tab2Params.id = "";
   }

  
}


