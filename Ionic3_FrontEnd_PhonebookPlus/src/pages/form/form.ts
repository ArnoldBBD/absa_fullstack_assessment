import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,Events } from 'ionic-angular';
import { Phonebook } from '../../models/phonebook';
import { LoadingOverlay } from '../../providers/LoadingOverlay';
import { PhonebookProvider } from '../../providers/http/phonebook';
import { ContactsProvider } from '../../providers/http/contacts';
import { HomePage } from '../home/home';
import { Contact } from '../../models/contact';


/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {

 
  showMask = true;

  private _phonebook: Phonebook;
  private _contact: Contact;

  private _phonebooks: Phonebook[] = [];
  private _home: HomePage;

  private _formType: string = '';
  gender: string = "f";

  private masks: any;
  

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private _loading: LoadingOverlay,
     private _alertCtrl: AlertController,
     private _phonebookProvider: PhonebookProvider,
     private _contactsProvider: ContactsProvider,
      private events: Events) {

    this._phonebook = new Phonebook();
    this._contact = new Contact();

    this._formType = this.navParams.get("FormType");

    if(this._formType == "Edit Phonebook")
      this._phonebook = this.navParams.get("_phonebook");
    else if(this._formType == "Edit Contact")
      this._contact = this.navParams.get("_contact");

    this.masks = {phoneNumber: ['(', /[0]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]};

  }

 

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPage');
   
  }

  ionViewDidEnter()
  {
    this.getPhonebooks();
  }

  getPhonebooks(){
    this._phonebookProvider._phonebookItems.subscribe((phonebooks) => {
    this._phonebooks = phonebooks;
      
     });
  }
  



  createPhonebook() {
 
    let popup = this._loading.ShowMessage('Saving...');


     if(this._phonebook.name == null ||
        this._phonebook.description == null ||
        this._phonebook.name == "" ||
        this._phonebook.description == "")
      {
        popup.dismiss();
       alert('Enter all required fields');
        return;
      }
  

      console.log(JSON.stringify(this._phonebook));

      this._phonebookProvider.createPhonebook(this._phonebook).subscribe(() => {

        this._phonebook.name = '';
        this._phonebook.description = '';
        
        popup.dismiss();
        this.navParams.get("HomePage").loadPhonebooks();
        this.navCtrl.pop();
      
      }, (err1) => { console.log(err1) });
  }//end of create


  createContact() {
 
    let popup = this._loading.ShowMessage('Saving...');


     if(this._contact.contactName == null ||
        this._contact.phoneNumber == null ||
        this._contact.phonebookId == null)
      {
        popup.dismiss();
       alert('Enter all required fields');
        return;
      }
  

      console.log(JSON.stringify(this._contact));

      //this._contact.phoneNumber = this._contact.phoneNumber.replace(/[( )-]/g,"");

      this._contactsProvider.createContact(this._contact).subscribe(() => {

        this.events.publish('change-tab',1,this.navParams.get("Type"),this._contact.phonebookId);
        this._contact.phonebookId = '';
        this._contact.contactName = '';
        this._contact.phoneNumber = '';
        
        popup.dismiss();
        this.navParams.get("AllContactsPage").load();
        
      
      }, (err1) => { console.log(err1) });
  }//end of create


  //alert function
  alert(msg: string){
    let alert = this._alertCtrl.create({
      title: 'Invalid input',
      message: msg,
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        },
      ]
    });

    alert.present();
  }


  updatePhonebook() {
 
    let popup = this._loading.ShowMessage('Saving...');
  
     if(this._phonebook.name == null || this._phonebook.description == null || this._phonebook.name == "" || this._phonebook.description == "")
      {
        popup.dismiss();
       alert('Enter all required fields');
        return;
      }
  

      console.log(JSON.stringify(this._phonebook));


        this._phonebookProvider.updatePhonebook(this._phonebook).subscribe(() => {

        
        this._phonebook.name = '';
        this._phonebook.description = '';
        
        popup.dismiss();
        this.navParams.get("HomePage").loadPhonebooks();
        this.navCtrl.pop();
      
      }, (err1) => { console.log(err1) });
  }


  
  updateContact() {
 
    let popup = this._loading.ShowMessage('Saving...');
  
     if(this._contact.contactName == null || this._contact.phoneNumber == null || this._contact.phonebookId == null ||
         this._contact.contactName == "" || this._contact.phoneNumber == "" || this._contact.phonebookId == "")
      {
        popup.dismiss();
       alert('Enter all required fields');
        return;
      }
  

      console.log(JSON.stringify(this._contact));


        this._contactsProvider.updateContact(this._contact).subscribe(() => {
        this.events.publish('change-tab',1,this.navParams.get("Type"),this._contact.phonebookId);
        this._contact.contactName = '';
        this._contact.phoneNumber = '';
        this._contact.phonebookId = '';
        
        popup.dismiss();
      
      }, (err1) => { console.log(err1) });
  }

 

}
