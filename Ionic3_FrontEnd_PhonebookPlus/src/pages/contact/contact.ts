import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, Nav,App, Events } from 'ionic-angular';
import { FormPage } from '../form/form';
import { ContactsProvider } from '../../providers/http/contacts';
import { Contact } from '../../models/contact';
import { LoadingOverlay } from '../../providers/LoadingOverlay';
import { Util } from '../../providers/util/util';
import { CallNumber } from '@ionic-native/call-number';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  private masks: any;
  type: string = "";
  id: string = "";
  new: boolean = false;

  private _contactEntries: Contact[] = [];

  constructor(public navCtrl: NavController,
     private _contactsProvider: ContactsProvider,
      private _loading: LoadingOverlay,
      private _callNumber: CallNumber,
       private _alrtCtrl: AlertController,
       private _navParams: NavParams,
       private _nav: Nav,
       private _app: App,
       private events: Events) {
    this.masks = {phoneNumber: ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]};

  }




  ionViewDidEnter(){
    this.load(false);
  }


  load(_IsInPhonebook?: boolean){

    if(!_IsInPhonebook)
    {
    this.id = this._navParams.get('id');
    this.type = this._navParams.get('type');
    }

    
    if(this.type == 'ByPhonebook')
    {
      this.loadAllContactsByPhonebook(this.id);
    }
    else
      this.loadAllContacts();

  }


  goToCreateForm(_type: string) {
    this.navCtrl.push(FormPage, {"AllContactsPage": this, "FormType": _type, "PhonebookID": this.id, "Type": this.type});
  }

  goToEditForm(c: Contact) {
    this.navCtrl.push(FormPage, {"ContactPage": this, "FormType": "Edit Contact", "_contact": c, "PhonebookID": this.id, "Type": this.type});
  }



  async loadAllContacts() {

    let popup = this._loading.ShowMessage('Loading...');

    this._contactsProvider.getContactEntries().subscribe((contacts) => {
     
    this._contactEntries = contacts;

    this._contactEntries.forEach(contact => {
     contact.avatar = Util.pathAvatar();
    });

    popup.dismiss();
      
     });
  }

  async loadAllContactsByPhonebook(id: string) {

    let popup = this._loading.ShowMessage('Loading...');

    this._contactsProvider. getContactEntriesByPhonebook(id).subscribe((contacts) => {
     
    if(contacts != null)
      this._contactEntries = contacts;

    this._contactEntries.forEach(contact => {
     contact.avatar = Util.pathAvatar();
    });

    popup.dismiss();
      
     });

     
  }

  callNumber(c: Contact) {

    var number = c.phoneNumber.replace(/[( )-]/g,"");
  
      this._callNumber.callNumber(number, true).then(res => console.log('Launched dialer!', res)).catch(err => console.log('Error launching dialer', err));  

  }


  delete(c: Contact) {

    let alert = this._alrtCtrl.create({
      title: 'Delete contact',
      message: 'Are you sure you want to delete this contact?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {

            let popup = this._loading.ShowMessage('Loading...');
            
            this._contactsProvider.deleteContact(c.id).subscribe(() =>{
              popup.dismiss();
              //this.type = this._navParams.get('type');
              if(this.type != '')
              this.load(true);
              else
              this.load(false);
              
            },(err) =>{
                console.log(err);
            });

          }
        }
      ]
    });

    alert.present();

  }

  getContacts(ev: any) {
    // Reset items back to all of the items
    // set val to the value of the searchbar
    let val = ev.target.value;
    console.log(val);
   
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {

      this._contactsProvider.getContactBySearch(val,this.id).subscribe((contacts) => {
     
        this._contactEntries = contacts;
    
        this._contactEntries.forEach(contact => {
         contact.avatar = Util.pathAvatar();
        });

      })
    }
    else if(this.id.length > 0){
      this.loadAllContactsByPhonebook(this.id);
    }
    else 
    this.load();
 
  }


}
